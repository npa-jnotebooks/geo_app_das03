##############################################################################
################################ Libraries ###################################
###############################################################################
import os
import numpy as np
import sys
import h5py
import math
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

###############################################################################
################################ Functions ####################################
###############################################################################

def extractAttribut(fileName):
    """
    Parameters
    ----------
    fileName : STRING
        DESCRIPTION: file path [file in h5 format]

    Returns
    -------
     
    attributesDict : Dictionary
        DESCRIPTION. It contains:
            
                    -Attributes of each zone
                    
                    -Attributes wall 
                    
                    -list zones
    dataDict : Dictionary
        DESCRIPTION. It contains:
            
                    -Structure file

    """
    def h5py_dataset_iterator(g, prefix=''):
        for key in g.keys():
            item = g[key]
            path = '{}/{}'.format(prefix, key)
            if isinstance(item, h5py.Dataset): # test for dataset
                # for it in item:
                yield (path, item)
                # yield (path, item)
            elif isinstance(item, h5py.Group): # test for group (go down)
                for it in h5py_dataset_iterator(item, path):
                    yield it
                
    attributesDict={}
    dataDict={}
    listZone=[]
    with h5py.File(fileName, 'r') as f:
        i=0
        
        for (path, dset) in h5py_dataset_iterator(f):
            dataDict["att"+str(i)]  =path.split("/")
            listZone.append(dataDict["att"+str(i)][3])
            i=i+1
        
        listZone=list(dict.fromkeys(listZone))
        listZone=["Zone"+str(i) for i in np.arange(1,len(listZone))]
        
        for j in range(len(listZone)):
            attributesDict["Attributes_Zone"+str(j+1)]= dict (f[dataDict['att0'][1]]['Source1']['Zone'+str(j+1)].attrs.items())
            if 'FreqRes' in attributesDict["Attributes_Zone"+str(j+1)]:
                attributesDict["Attributes_Zone"+str(j+1)]['BlockRate']=attributesDict["Attributes_Zone"+str(j+1)]['FreqRes']
                

        attributesDict['Attributes_wall']= dict (f[dataDict['att0'][1]]['Source1'].attrs.items())
        
        if 'FreqRes' in attributesDict['Attributes_wall']:
            attributesDict['Attributes_wall']['BlockRate']=attributesDict['Attributes_wall']['FreqRes']
            
        attributesDict['Zones']=listZone
        

        
    print(" #### : Extract attributs [Done]")
    return attributesDict, dataDict


"""  Read data in different format "Raw", "StrainRate", "RealPart"  """


def readH5(fileName, startDistance=None,endDistance=None,TimeSamplingRate=1,SpatialSamplingRate=1,timeType="second",startTime=1,endTime=1, modePro="StrainRate"):
        """
    Parameters
    ----------
    fileName : STRING
        DESCRIPTION: file path in h5 format
    startDistance : FLOAT, optional
        DESCRIPTION. The default is None, start distance, if None take the first distance
    endDistance : FLOAT, optional
        DESCRIPTION. The default is None, end distance, if None take the last distance
    TimeSamplingRate : INT, optional
        DESCRIPTION. The default is 1. Performed with picking method
    SpatialSamplingRate : INT, optional
        DESCRIPTION. The default is 1. Performed with picking method
    timeType : FLOAT, optional
        DESCRIPTION. The default is "second". If "second" we select data according to recording block, if "timeStamp" we use the UTC timeStamp
    startTime : INT, optional
        DESCRIPTION. The default is 1. first block to read. It can be used in both modes "second" and "timeStamp"
    endTime : INT, optional
        DESCRIPTION. The default is 1. last block to read. It can be used in both modes "second" and "timeStamp"
    modePro : TYPE, optional
        DESCRIPTION. The default is "StrainRate".Only "StrainRate", "strain", "FFT" and "Raw" can be read

    Returns
    -------
    attributesDict : Dictionary
        DESCRIPTION. It contains:
            
                    -Attributes of each zone
                    
                    -Attributes wall 
    dataDict : Dictionary
        DESCRIPTION. It contains:
            
                    -file structure for each sub-zone
                    
                    -data ['StrainRate' or 'Strain' or 'Raw' or 'FFT']
                    
                    -distance :selected distance
                    
                    -timeStamp :all timeStamp
                    
                    -extractedTimeStamp : selected timeStamp
                    
                    -timeBlock :concatenate all time inside blocks
                    
                    -frequency : computed from PRF
    """
        attributesDict,dataDict=extractAttribut(fileName)
        # print(attributesDict['Attributes_Zone1'])
        
        if len(attributesDict['Zones'])==1:
            multiZone=False
        else:
            multiZone=True


        with h5py.File(fileName, "r") as dataOut:

            data = dataOut[dataDict['att0'][1]]
            data=data[dataDict['att0'][2]]
            
            listStartEndIndex=[]

            dataDict['timeStamp']= np.array(data['time'],dtype=float)
            
            for zone in attributesDict['Zones']:
                dataDict['distance_'+zone]= np.arange(attributesDict['Attributes_'+zone]['Extent'][0],1+attributesDict['Attributes_'+zone]['Extent'][1])*attributesDict['Attributes_'+zone]['Spacing'][0]
                dataDict['distance_'+zone]=dataDict['distance_'+zone]+attributesDict['Attributes_'+zone]['Origin'][0]
            if startDistance==None:
                startDistance=dataDict['distance_'+attributesDict['Zones'][0]][0]
            else:
                pass
            
            if endDistance==None:
                endDistance=dataDict['distance_'+attributesDict['Zones'][-1]][-1] #-2 to not include time
            else:
                pass
            
            for zone in attributesDict['Zones']:

                

                if (dataDict['distance_'+zone][0]<=startDistance<dataDict['distance_'+zone][-1]) and (dataDict['distance_'+zone][0]<=endDistance<=dataDict['distance_'+zone][-1]):
                    dis_start=int((startDistance-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])
                    dis_end=int((endDistance-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])
                    listStartEndIndex.append((dis_start,dis_end+1))

                    
                elif (dataDict['distance_'+zone][0]<=startDistance<dataDict['distance_'+zone][-1]) and (endDistance>dataDict['distance_'+zone][-1]):
              
                    dis_start=int((startDistance-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])
                    dis_end=(attributesDict['Attributes_'+zone]['Extent'][1])*attributesDict['Attributes_'+zone]['Spacing'][0]
                    dis_end=int((dis_end-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])
                    listStartEndIndex.append((dis_start,dis_end+1))                    
                    
                elif (dataDict['distance_'+zone][0]>startDistance) and (dataDict['distance_'+zone][0]<=endDistance<=dataDict['distance_'+zone][-1]):

                    dis_start=(attributesDict['Attributes_'+zone]['Extent'][0])*attributesDict['Attributes_'+zone]['Spacing'][0]
                    dis_start=int((dis_start-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])
                    dis_end=int((endDistance-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])
                    listStartEndIndex.append((dis_start,dis_end+1))                     
 
                elif (dataDict['distance_'+zone][0]>startDistance)  and (endDistance>dataDict['distance_'+zone][-1]):

                    dis_start=(attributesDict['Attributes_'+zone]['Extent'][0])*attributesDict['Attributes_'+zone]['Spacing'][0]
                    dis_start=int((dis_start-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0]) 
                    dis_end=(attributesDict['Attributes_'+zone]['Extent'][1])*attributesDict['Attributes_'+zone]['Spacing'][0]
                    dis_end=int((dis_end-dataDict['distance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])                       
                    
                    listStartEndIndex.append((dis_start,dis_end+1)) 
                    
                elif (dataDict['distance_'+zone][0]>startDistance)  and (dataDict['distance_'+zone][0]>endDistance):
                    listStartEndIndex.append((0,0)) 

                elif (dataDict['distance_'+zone][-1]<startDistance)  and (dataDict['distance_'+zone][-1]<endDistance):
                    listStartEndIndex.append((0,0)) 
                    
                    
            if len(listStartEndIndex) ==0:
                print("########################################################################################")
                print("       WARNING!!!  :the selected distance are outside the recorded data     ")
                print("       WARNING!!!  :there is no selected data     ")
                print("########################################################################################")  
                return attributesDict,dataDict
                sys.exit()

            
            
            u=0
            for zone in attributesDict['Zones']:
                if endDistance > dataDict['distance_'+zone][0] :
                    pass
                else:
                    listStartEndIndex[u]=(0,0)   
                u+=1
            
            v=0
            for zone in attributesDict['Zones']:
                if  startDistance < dataDict['distance_'+zone][-1]:
                    pass
                else:
                    listStartEndIndex[v]=(0,0)     
                    # dataDict['distance_'+zone]=np.array([0])
                v+=1  
                #-------Time---------#
            if timeType=='second':
                if startTime==None:
                    sigf=0
                else:
                    # sigf=int(startTime*1000/(attributesDict['Attributes_wall']['BlockRate'][0]))
                    sigf=int(startTime*(attributesDict['Attributes_wall']['BlockRate'][0])/1000)

                if endTime==None:
                    sigl=data['Zone1'][dataDict['att0'][4]].shape[0]
                else:
                    # sigl=math.ceil(endTime*1000/(attributesDict['Attributes_wall']['BlockRate'][0]))
                    sigl=math.ceil(endTime*(attributesDict['Attributes_wall']['BlockRate'][0])/1000)
            

            elif timeType=='timeStamp':
                if startTime==None:
                    sigf=0
                elif endTime==None:
                    sigl=int(data['Zone1'][dataDict['att0'][4]].shape[0])
                else:
                    sigf=abs(dataDict['timeStamp']-startTime).argmin()
                    sigl=abs(dataDict['timeStamp']-endTime).argmin()
                    if dataDict['timeStamp'][sigf]>startTime:
                        sigf=sigf-1
                    else:
                        pass
                    if dataDict['timeStamp'][sigl]>endTime:
                        sigl=sigl-1
                        
                    
                    sigl=sigl+1
                    


        
            dataDict['extractedTimeStamp']=dataDict['timeStamp'][sigf:sigl]
            
            #-------Data---------#

            k=0
          
        
            if modePro in ['StrainRate','Strain','FFT','Strain', 'RealPart']:
                for zone in attributesDict['Zones']:
                    # if zone in dataDict['att'+str(k)]:
                    if (data[zone][modePro].ndim !=3): 
                    
                        dataDict[dataDict['att'+str(k)][4]+'_'+zone]=np.array(data[zone][dataDict['att'+str(k)][4]][::TimeSamplingRate,listStartEndIndex[k][0]:listStartEndIndex[k][1]:SpatialSamplingRate],dtype=np.float32)
                        dataDict[dataDict['att'+str(k)][4]+'_'+zone]=dataDict[dataDict['att'+str(k)][4]+'_'+zone][np.newaxis,:,:]
                    else:
                        dataDict[dataDict['att'+str(k)][4]+'_'+zone]=np.array(data[zone][dataDict['att'+str(k)][4]][sigf:sigl,::TimeSamplingRate,listStartEndIndex[k][0]:listStartEndIndex[k][1]:SpatialSamplingRate],dtype=np.float32)
                    dataDict['extractedDistance_'+zone]=dataDict['distance_'+zone][listStartEndIndex[k][0]:listStartEndIndex[k][1]:SpatialSamplingRate]
                    k+=1
                    
            
            
            else:
                print("###############################################################")
                print("       Please check what kind of file you want to read         ")
                print("Only the following format exist : Raw, StrainRate, strain, FFT ")
                print("###############################################################")
                # exit()

    
            stepTime=1/(attributesDict["Attributes_"+'Zone1']['Spacing'][1]/1000)/(attributesDict["Attributes_"+'Zone1']['BlockRate']/1000)

            dataDict['timeBlock']=np.arange(0,stepTime)/(1/(attributesDict["Attributes_"+'Zone1']['Spacing'][1]/1000)-1)
            dataDict['frequence'] = stepTime*np.arange(0,dataDict['timeBlock'].shape[0])/(dataDict['timeBlock'].shape[0] -1)


        
        for zone in attributesDict['Zones']:
            if dataDict['extractedDistance_'+zone].shape[0]==0:
                print("########################################################################################")
                print("       WARNING!!!  :the selected distance in "+zone+" are outside the recorded data     ")
                print("########################################################################################")   
                
        if dataDict['extractedTimeStamp'].shape[0]==0:
            print("#########################################################################")
            print("       WARNING!!!  :the selected time are outside the recorded data    ")
            print("#########################################################################")                   

                

        if multiZone==False:
            dataDict['distance']=dataDict['distance_Zone1'].copy()
            dataDict['extractedDistance']=dataDict['extractedDistance_Zone1'].copy()
            del dataDict['distance_Zone1']
            del dataDict['extractedDistance_Zone1']
            attributesDict['Attributes']=attributesDict['Attributes_Zone1'].copy()
            del attributesDict['Attributes_Zone1'] 
            if modePro in ['StrainRate','Strain','FFT','RealPart']: 
                dataDict[dataDict['att'+str(0)][4]]=dataDict[dataDict['att'+str(0)][4]+"_Zone1"].copy()
                del dataDict[dataDict['att'+str(0)][4]+"_Zone1"]

        else:
            pass
            
        
        print(" #### : Read file [Done]") 
        return attributesDict,dataDict


def removeRedundancy(attributesDict,dataDict, modePro):
    ################
    
    BlockRate=1000/(attributesDict['Attributes_wall']['BlockRate'][0])
    if len(attributesDict['Zones'])==1:
        multiZone=False
    else:
        multiZone=True 

    if multiZone==False:      
        y=dataDict[modePro].shape[1]

        toRemove=(y-BlockRate/(attributesDict["Attributes"]['Spacing'][1]/1000) )
        dataDict[modePro]=dataDict[modePro][:,int(toRemove/2):int(y-int(toRemove/2)),:] 
        
    elif multiZone==True:
        for zone in attributesDict['Zones']:
            y=dataDict[modePro+"_"+zone].shape[1]
            toRemove=(y-BlockRate/(attributesDict["Attributes_"+zone]['Spacing'][1]/1000) )
            

            dataDict[modePro+"_"+zone]=dataDict[modePro+"_"+zone][:,int((toRemove/2)):int(y-(toRemove/2)),:]         
    

    return attributesDict,dataDict

""" concatenate data """





def concatH5(dataDict,attributesDict,timeType='second',startTime=0,endTime=1,modePro="StrainRate"):  
    """
    

    Parameters
    ----------
    dataDict : DICTIONARY
        DESCRIPTION.
    attributesDict : DICTIONARY
        DESCRIPTION.
    timeType : STRING
        DESCRIPTION.
    startTime : FLOAT, optional
        DESCRIPTION. The default is 0.
    endTime : FLOAT, optional
        DESCRIPTION. The default is 1.
    startDistance : FLOAT, optional
        DESCRIPTION. The default is None.
    endDistance : FLOAT, optional
        DESCRIPTION. The default is None.
    modePro : STRING, optional
        DESCRIPTION. The default is "StrainRate".

    Returns
    -------
    None.

    """
    if len(attributesDict['Zones'])==1:
        multiZone=False
    else:
        multiZone=True 
        
        
        
    if multiZone==False:  
        if timeType=='second':
    
            if startTime==None:
                sigf=0
            else:
                sigf=startTime
            if endTime==None:
                sigl=dataDict[dataDict['att0'][4]].shape[0]
            else:
                sigl=endTime
               
                
            
            BlockRate=1000/(attributesDict['Attributes_wall']['BlockRate'][0])
            timeStartIndex=int(sigf*dataDict[dataDict['att0'][4]].shape[1]/(BlockRate))  ###BlockRate
            timeLastIndex=int(sigl*dataDict[dataDict['att0'][4]].shape[1]/(BlockRate))     ###BlockRate


        elif timeType=='timeStamp':
            if startTime==None:
                sigf=0
            elif endTime==None:
                sigl=dataDict[dataDict['att0'][4]].shape[0]
            else:
                
                sigf=startTime-dataDict['extractedTimeStamp'][0]
                sigl=endTime-dataDict['extractedTimeStamp'][-1]
              
           

            timeStartIndex=int(sigf*dataDict[dataDict['att0'][4]].shape[1])  ###BlockRate
            timeLastIndex=int((dataDict['extractedTimeStamp'].shape[0]-sigl)*dataDict[dataDict['att0'][4]].shape[1])     ###BlockRate   
            
        
        startIndexBlock=0             
        lastIndexBlock=int(dataDict[dataDict['att0'][4]].shape[1] )
        
        
        

        if modePro in ['StrainRate','RealPart','Strain']:
            if (modePro == 'RealPart') or (modePro == 'Strain'):
                for ll in np.arange(1,dataDict[dataDict['att0'][4]].shape[0]):
                    # offset=dataDict[dataDict['att0'][4]][ll-1,lastIndexBlock-1,:]-dataDict[dataDict['att0'][4]][ll,startIndexBlock-1,:]
                    offset=dataDict[dataDict['att0'][4]][ll-1,lastIndexBlock-1,:]-dataDict[dataDict['att0'][4]][ll,0,:]
                    dataDict[dataDict['att0'][4]][ll,:,:]=dataDict[dataDict['att0'][4]][ll,:,:]+offset
                

            dataDict[dataDict['att0'][4]]=dataDict[dataDict['att0'][4]][:,startIndexBlock:lastIndexBlock,:]
            
         
            
        
        x=dataDict[dataDict['att0'][4]].shape[0]
        y=dataDict[dataDict['att0'][4]].shape[1]
        z=dataDict[dataDict['att0'][4]].shape[2]  
        
        
        if modePro in ['StrainRate','RealPart','Strain']:
            dataDict[dataDict['att0'][4]]=np.reshape(dataDict[dataDict['att0'][4]],(x*y,z))


            dataDict[dataDict['att0'][4]]=dataDict[dataDict['att0'][4]][timeStartIndex:timeLastIndex,:]


    elif multiZone==True:
        
        
        if timeType=='second':
    
            if startTime==None:
                sigf=0
            else:
                sigf=startTime
            if endTime==None:
                sigl=dataDict[dataDict['att0'][4]].shape[0]
            else:
                sigl=endTime
               
                
            
            BlockRate=1000/(attributesDict['Attributes_wall']['BlockRate'][0])
            timeStartIndex=int(sigf*dataDict[dataDict['att0'][4]+"_Zone1"].shape[1]/(BlockRate))  ###BlockRate
            timeLastIndex=int(sigl*dataDict[dataDict['att0'][4]+"_Zone1"].shape[1]/(BlockRate))     ###BlockRate


        elif timeType=='timeStamp':
            if startTime==None:
                sigf=0
            elif endTime==None:
                sigl=dataDict[dataDict['att0'][4]].shape[0]
            else:
                
                sigf=startTime-dataDict['extractedTimeStamp'][0]
                sigl=endTime-dataDict['extractedTimeStamp'][-1]
              
           

            timeStartIndex=int(sigf*dataDict[dataDict['att0'][4]].shape[1])  ###BlockRate
            timeLastIndex=int((dataDict['extractedTimeStamp'].shape[0]-sigl)*dataDict[dataDict['att0'][4]].shape[1])     ###BlockRate          

        
        startIndexBlock=0             
        lastIndexBlock=int(dataDict[dataDict['att0'][4]+"_Zone1"].shape[1] )
        

        for zone in attributesDict['Zones']:
            if modePro in ['StrainRate','RealPart','Strain']:
                
                if (modePro == 'RealPart') or (modePro == 'Strain'):
                    for ll in np.arange(1,dataDict[dataDict['att0'][4]+"_"+zone].shape[0]):
                        offset=dataDict[dataDict['att0'][4]+"_"+zone][ll-1,lastIndexBlock-1,:]-dataDict[dataDict['att0'][4]+"_"+zone][ll,0,:]
                        dataDict[dataDict['att0'][4]+"_"+zone][ll,:,:]=dataDict[dataDict['att0'][4]+"_"+zone][ll,:,:]+offset
                
                
                dataDict[dataDict['att0'][4]+"_"+zone]=dataDict[dataDict['att0'][4]+"_"+zone][:,startIndexBlock:lastIndexBlock,:]
                
            
            x=dataDict[dataDict['att0'][4]+"_"+zone].shape[0]
            y=dataDict[dataDict['att0'][4]+"_"+zone].shape[1]
            z=dataDict[dataDict['att0'][4]+"_"+zone].shape[2]
           
            
            if modePro == 'StrainRate':
                dataDict[dataDict['att0'][4]+"_"+zone]=np.reshape(dataDict[dataDict['att0'][4]+"_"+zone],(x*y,z))
                dataDict[dataDict['att0'][4]+"_"+zone]=dataDict[dataDict['att0'][4]+"_"+zone][timeStartIndex:timeLastIndex,:]

            elif (modePro == 'RealPart') or (modePro == 'Strain'):
                dataDict[dataDict['att0'][4]+"_"+zone]=np.reshape(dataDict[dataDict['att0'][4]+"_"+zone],(x*y,z))
    
    
    print(" #### : Concatenate file [Done]") 
    
    return attributesDict, dataDict

def writeH5(output_fileName,dataDict,attributesDict,modePro="StrainRate"): 
    
    """
    Parameters
    ----------
    output_filename : STRING
        DESCRIPTION.
    dataDict : DICTIONARY
        DESCRIPTION.
    attributesDict : DICTIONARY
        DESCRIPTION.
    modePro : STRING, optional
        DESCRIPTION. The default is "StrainRate". can take ['StrainRate', 'RealPart', 'FFT']

    Returns
    -------
    Nothing.

    """

    if len(attributesDict['Zones'])==1:
        multiZone=False
    else:
        multiZone=True     
    
                     
    
    mon_fichier=h5py.File(output_fileName,'w')         
    mon_groupe=mon_fichier.create_group(dataDict['att0'][1])
    mon_subgroupe_01=mon_groupe.create_group(dataDict['att0'][2])
    mon_subgroupe_01.create_dataset("time",data=dataDict["extractedTimeStamp"])

    if multiZone==True: 
        list_subgroups=[]
        if modePro in ['StrainRate','RealPart','FFT', 'Strain']:
            
            l=0
            for zone in attributesDict['Zones']:
                list_subgroups.append(mon_subgroupe_01.create_group(dataDict['att'+str(l)][3]))

                list_subgroups[l].create_dataset(dataDict['att0'][4],data=dataDict[dataDict['att0'][4]+'_'+zone])
                
        
                for key, value in attributesDict['Attributes_'+zone].items():
                    list_subgroups[l].attrs[key]=value                    
                l=l+1

                
        print(" #### : Open data h5 format [Done]")
        print(" #### : Creat groups and dataSets  [Done]")    
    
        for key, value in attributesDict['Attributes_wall'].items():
            mon_subgroupe_01.attrs[key]=value
            

        l=0
        
        for zone in attributesDict['Zones']:
            ext2=list_subgroups[l].attrs["Extent"]
            if len(dataDict['extractedDistance_'+zone])==0:
                ext2[0]=0
                ext2[1]=0
            else:
    
                ext2[0]= ((dataDict['extractedDistance_'+zone][0])/attributesDict['Attributes_'+zone]['Spacing'][0])-1
                ext2[1]= ((dataDict['extractedDistance_'+zone][-1])/attributesDict['Attributes_'+zone]['Spacing'][0])-1   
            
            
          
            list_subgroups[l].attrs.modify('Extent',ext2) 
            l=l+1  

    elif multiZone==False: 
        mon_subgroupe_02=mon_subgroupe_01.create_group(dataDict['att0'][3])
        
        if modePro in ['StrainRate','RealPart','FFT', 'Strain']:
            # mon_subgroupe_02.create_dataset(dataDict['att0'][4],data=dataDict[dataDict['att0'][4]])
            mon_subgroupe_02.create_dataset(modePro,data=dataDict[dataDict['att0'][4]])
                   

    
        for key, value in attributesDict['Attributes'].items():
            mon_subgroupe_02.attrs[key]=value                 

        for key, value in attributesDict['Attributes_wall'].items():
            mon_subgroupe_01.attrs[key]=value
            
        print(" #### : Open data h5 format [Done]")
        print(" #### : Creat groups and dataSets  [Done]") 
        
        
        ext2=mon_subgroupe_02.attrs["Extent"]
        if len(dataDict['extractedDistance'])==0:
            ext2[0]=0
            ext2[1]=0
        else:

            ext2[0]= ((dataDict['extractedDistance'][0])/attributesDict['Attributes']['Spacing'][0])-1
            ext2[1]= ((dataDict['extractedDistance'][-1])/attributesDict['Attributes']['Spacing'][0])-1   
            
            
          
 
           
    mon_fichier.close()
    print(" #### : Close file  [Done]") 
    print(" #### : Process is finished ")   
    print(" #### : Write file [Done]") 
    
def plotMatrix(data,dataDict,attributesDict,output_figure ,limMin= 1000,limMax= 1000,cmap="gray",startTime=0,endTime=4,axesX='time',axesY="distance",invertAxis=False,zone="_Zone1",title="SR"):

    ##invert axis or not
    if invertAxis==True:
        matrixToPlot=data[:,::-1].T
    else:
        matrixToPlot=data
        
        
        
    # PRF=attributesDict['Attributes']['PulseRateFreq'][0]/1000 

    if startTime==None:
        sigf=0
    else:
        sigf=startTime
    if endTime==None:
        # sigl=sigf+(dataDict[dataDict['att0'][4]].shape[0]/PRF)
        sigl=sigf+(dataDict['extractedTimeStamp'].shape[0])
    else:
        sigl=endTime
    
    #print(sigl)
    # sigl=311
    figure, axes = plt.subplots(1,figsize=(13,6),sharex=True,sharey=True)
    figure.subplots_adjust(left=0.16, bottom=0.077, right=0.88, top=None, wspace=None, hspace=0.05)

    if len(attributesDict['Zones'])==1:
        multiZone=False
    else:
        multiZone=True 
        
        
        
    if multiZone==False:  
        temps=np.arange(sigf,sigl+(attributesDict['Attributes']['Spacing'][1]/1000),(attributesDict['Attributes']['Spacing'][1]/1000))
        distance=dataDict['extractedDistance']
    else:
        temps=np.arange(sigf,sigl+(attributesDict['Attributes'+zone]['Spacing'][1]/1000),(attributesDict['Attributes'+zone]['Spacing'][1]/1000))
        distance=dataDict['extractedDistance'+zone]
        

        
    # temps=np.arange((1/(attributesDict['Attributes']['Spacing'][1]/1000))*startTime,(1+(1/(attributesDict['Attributes']['Spacing'][1]/1000)))*(endTime))/(1/(attributesDict['Attributes']['Spacing'][1]/1000))
    
    colbar=axes.imshow(matrixToPlot,vmin=limMin,vmax=limMax,
                          extent =[min(temps),max(temps),max(distance),min(distance)], 
                          aspect= 'auto',origin='lower', cmap=cmap,interpolation='nearest')

    

    divider = make_axes_locatable(axes)
    ca_wf = divider.append_axes("right", size="1%", pad=0.1)
    figure.colorbar(colbar,cax=ca_wf) 
    
    axes.set_title(title)  

    axes.set_ylabel('Distance [m]')
    axes.set_xlabel('Time [s]')
    axes.grid(False)
    
    # if axesX=='timeStamp':
       
    #     waterfallTicks=list(np.arange(0,0+len(dataDict['extractedTimeStamp'])/4+len(dataDict['extractedTimeStamp']),len(dataDict['extractedTimeStamp'])/4))
    #     stepTimeStamp=(dataDict['extractedTimeStamp'][-1]-dataDict['extractedTimeStamp'][0])/4
    #     waterfallTicksLabels=np.arange(dataDict['extractedTimeStamp'][0],dataDict['extractedTimeStamp'][-1]+1,stepTimeStamp)
    #     axes.set_xticks(waterfallTicks)
    #     axes.set_xticklabels(secondToDate(waterfallTicksLabels))
    # else:
    #     pass
    
    plt.savefig(output_figure)   
    
    figure.show()     
   
        
###############################################################################
